import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "@fontsource/poppins/700.css"
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { DAppProvider } from "@usedapp/core";

const colors = {
  brand: {
    50: "#deffde",
    100: "#afffaf",
    200: "#7dff7e",
    300: "#4bff4c",
    400: "#1aff1a",
    500: "#00e600",
    600: "#00b300",
    700: "#008000",
    800: "#004e00",
    900: "#001c00",
  }
};
const config = {
  initialColorMode: "dark",
  useSystemColorMode: false,
  fonts: {
    heading: "Poppins",
    body: "Poppins",
  },
};

const theme = extendTheme({ colors, config });

const rootElement = document.getElementById("root");
ReactDOM.render(
  <DAppProvider config={{}}>
    <ChakraProvider theme={theme}>
      <App />
    </ChakraProvider>
  </DAppProvider>,

  rootElement
);

