 
export const columnsData2 = [
  {
    Header: "TOKEN",
    accessor: "token",
  },
  {
    Header: "Name",
    accessor: "dao_name",
  },
  {
    Header: "Initial Tokens",
    accessor: "initial_tokens",
  },
  {
    Header: "Current Tokens",
    accessor: "current_balance",
  }, 
  {
    Header: "Initial Balance",
    accessor: "initial_balance",
  }, 
  {
    Header: "REVENUE",
    accessor: "usd_balance",
  },
];
