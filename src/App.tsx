import React, { useEffect } from "react";
import {
    chakra,
    HStack,
    Box,
    Flex,
    IconButton,
    useColorModeValue,
    useDisclosure,
    useColorMode,
    Icon,
} from "@chakra-ui/react";
import { useViewportScroll } from "framer-motion";

import { FaMoon, FaSun } from "react-icons/fa";
import { columnsData2 } from "./variables/columnsData";
import Layout from "./components/Layout";
import { Logo } from "@choc-ui/logo";
import AccountModal from "./components/AccountModal";
import Card from "./components/Card/Card";
import CardBody from "./components/Card/CardBody";
import SearchTable2 from "./components/Tables/SearchTable2";
//ckey_0b02a76db9bb4809a54aa41972b
import Parse from 'parse/node';

Parse.serverURL = 'https://parseapi.back4app.com'; // This is your Server URL
// Remember to inform BOTH the Back4App Application ID AND the JavaScript KEY
Parse.initialize(
    'yaFK8cqJeUi3FVGjVL5JMP6rfC6mBAjAj2jk2jKK', // This is your Application ID
    '7UtWTLPid8CMaEyv8VvUPlDvKA5BZCnl3d7l82Oj', // This is your Javascript key
    'moU9hZkuHp4tGMu5Mgf2G9ABdIcMbifnzQu8I9ME' // This is your Master key (never use it in the frontend)
);

//https://mastercoin-api.ecokera.com/v1/43114/address/0xddaa2263bEc7F6F46cc89Df2667f39fcaF19ab4e/balances/?nft=true&limit=200
export default function App() {
    const { toggleColorMode: toggleMode } = useColorMode();
    const text = useColorModeValue("dark", "light");
    const SwitchIcon = useColorModeValue(FaMoon, FaSun);
    const bg = useColorModeValue("white", "gray.800");
    const ref = React.useRef();
    const [y, setY] = React.useState(0);
   // const { activateBrowserWallet, account } = useEthers();
   // const etherBalance = useEtherBalance(account);
    const { isOpen, onOpen, onClose } = useDisclosure();
    
    const cl = useColorModeValue("gray.800", "white");
    const mobileNav = useDisclosure(); 
    const [loading, setLoading] = React.useState(true);
    const [daodata, setDaodata] = React.useState<
                    Array<{
                        current_balance: number;
                        dao_name: string;
                        initial_tokens: number;
                        initial_balance: number;
                        token: string;
                        usd_balance: number;
                    }>
                >([])

    function handleConnectWallet() {
        //activateBrowserWallet();
    }

    function clearArray(array: Array<{
        current_balance: number;
        dao_name: string;
        initial_tokens: number;
        token: string;
        usd_balance: number;
    }>) {
        while (array.length) {
          array.pop();
        }
      }

    useEffect(() => {
        const DaoInvestments: Parse.Object = Parse.Object.extend('DaoInvestments');
        const query: Parse.Query = new Parse.Query('DaoInvestments');
          try { 
            query.find().then((results) => {
                
            for (const object of results) {
                // Access the Parse Object attributes using the .GET method
                const token: string = object.get('token')
                const initial_tokens: string = object.get('initial_tokens')
                const initial_balance: string = object.get('initial_balance')
                const current_balance: string = object.get('current_balance')
                const dao_name: string = object.get('dao_name')
                const usd_balance: string = object.get('usd_balance') 
                if(parseFloat(current_balance)>0){ 
                    daodata.push({ 
                        "current_balance": parseFloat(current_balance),
                        "dao_name": dao_name,
                        "initial_tokens": parseFloat(initial_tokens) ,
                        "initial_balance":parseFloat(initial_balance),
                        "token": token ,
                        "usd_balance": parseFloat(usd_balance),
                    })
                }
            }
            setDaodata(daodata);
            setLoading(false);
              }).catch((error) =>  {
                console.error('Error while fetching DaoInvestments', error);
              }); 

           
        } catch (error) {
            console.error('Error while fetching DaoInvestments', error);
        }
          
    }, [])
 
    const Feature = (props: any) => {
        return (
            <Flex>
                <Flex shrink={0}>
                    <Flex
                        alignItems="center"
                        justifyContent="center"
                        h={12}
                        w={12}
                        rounded="md"
                        bg={useColorModeValue("brand.500", "brand.500")}
                        color="white"
                    >
                        <Icon
                            boxSize={6}
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                        >
                            {props.icon}
                        </Icon>
                    </Flex>
                </Flex>
                <Box ml={4}>
                    <chakra.dt
                        fontSize="lg"
                        fontWeight="medium"
                        lineHeight="6"
                        color={useColorModeValue("gray.900", "gray.100")}
                    >
                        {props.title}
                    </chakra.dt>
                    <chakra.dd mt={2} color={useColorModeValue("gray.500", "gray.400")}>
                        {props.children}
                    </chakra.dd>
                </Box>
            </Flex>
        );
    };
    return (
        <Layout>
            <chakra.header
                transition="box-shadow 0.2s"
                bg={bg}
                borderTop="6px solid"
                borderTopColor="brand.400"
                w="full"
                overflowY="hidden"
                borderBottomWidth={2}
                borderBottomColor={useColorModeValue("gray.200", "gray.900")}
            >
                <chakra.div h="4.5rem" mx="auto" maxW="1200px">
                    <Flex
                        w="full"
                        h="full"
                        px="6"
                        alignItems="center"
                        justifyContent="space-between"
                    >
                        <Flex align="flex-start">
                            <HStack>
                                <Logo />
                                <chakra.h1 fontSize="xl" fontWeight="800" ml="2" >
                                    DAO Challenge 2022
                                </chakra.h1>
                            </HStack>
                            <IconButton
                                    size="md"
                                    fontSize="lg"
                                    aria-label={`Switch to ${text} mode`}
                                    variant="ghost"
                                    color="current"
                                    ml={{ base: "0", md: "3" }}
                                    onClick={toggleMode}
                                    icon={<SwitchIcon />}
                                />
                        </Flex>

                      { /* <Flex justify="flex-end" align="center" color="gray.400">
                            <HStack spacing="5" display={{ base: "none", md: "flex" }}>
                                <IconButton
                                    size="md"
                                    fontSize="lg"
                                    aria-label={`Switch to ${text} mode`}
                                    variant="ghost"
                                    color="current"
                                    ml={{ base: "0", md: "3" }}
                                    onClick={toggleMode}
                                    icon={<SwitchIcon />}
                                />
                                {
                                    account ? (
                                        <Box
                                            display="flex"
                                            alignItems="center"
                                            background="gray.700"
                                            borderRadius="xl"
                                            py="0"
                                        >
                                            <Box px="3">
                                                <Text color="white" fontSize="md">
                                                    {etherBalance && parseFloat(formatEther(etherBalance)).toFixed(3)} ETH
                                                </Text>
                                            </Box>
                                            <Button
                                                bg="gray.800"
                                                border="1px solid transparent"
                                                _hover={{
                                                    border: "1px",
                                                    borderStyle: "solid",
                                                    borderColor: "blue.400",
                                                    backgroundColor: "gray.700",
                                                }}
                                                borderRadius="xl"
                                                m="1px"
                                                px={3}
                                                height="38px"
                                                onClick={onOpen}
                                            >
                                                <Text color="white" fontSize="md" fontWeight="medium" mr="2">
                                                    {account &&
                                                        `${account.slice(0, 6)}...${account.slice(
                                                            account.length - 4,
                                                            account.length
                                                        )}`}
                                                </Text>
                                            </Button>
                                        </Box>
                                    ) : (
                                        <Button bg={"brand.500"} color={"white"} rounded="xl" onClick={handleConnectWallet}>Connect to a wallet</Button>
                                    )
                                }
                            </HStack>
                        </Flex>*/}
                    </Flex>
                </chakra.div>
            </chakra.header>
            {!loading  &&
                 <Box px={4} py={4} mx="auto">
                    <Card mx="auto" my={10}
                        px={8}
                        py={4}
                        rounded="lg"
                        shadow="lg" bg={useColorModeValue("gray.100", "gray.500")}>
                        <CardBody>
                            <SearchTable2 tableData={daodata} columnsData={columnsData2} />
                        </CardBody>
                    </Card>
                </Box>   }
            {/* <Flex
                bg={useColorModeValue("#F9FAFB", "gray.600")}
                p={20}
                w="auto"
                justifyContent="center"
                alignItems="center"
            >

                <Box py={12} bg={useColorModeValue("white", "gray.800")} rounded="xl">
                    <Box maxW="7xl" mx="auto" px={{ base: 4, lg: 8 }}>
                      <HStack>
                        <Flex
                            maxW="12rem"
                            bg={useColorModeValue("white", "gray.800")}
                            shadow="lg"
                            rounded="lg"
                            overflow="hidden"
                        >
                            <Box p={{ base: 4, md: 4 }}>
                                <Flex mt={3} alignItems="justified" justifyContent="space-between">
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        $220
                                    </chakra.h1>
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        &nbsp;&nbsp;&nbsp;AVALANCHE
                                    </chakra.h1>
                                </Flex>
                            </Box>
                        </Flex>
                        <Flex
                            maxW="12rem"
                            bg={useColorModeValue("white", "gray.800")}
                            shadow="lg"
                            rounded="lg"
                            overflow="hidden"
                        >
                            <Box p={{ base: 4, md: 4 }}>
                                <Flex mt={3} alignItems="justified" justifyContent="space-between">
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        $220
                                    </chakra.h1>
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        &nbsp;&nbsp;&nbsp;AVALANCHE
                                    </chakra.h1>
                                </Flex>
                            </Box>
                        </Flex>
                        <Flex
                            maxW="12rem"
                            bg={useColorModeValue("white", "gray.800")}
                            shadow="lg"
                            rounded="lg"
                            overflow="hidden"
                        >
                            <Box p={{ base: 4, md: 4 }}>
                                <Flex mt={3} alignItems="justified" justifyContent="space-between">
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        $220
                                    </chakra.h1>
                                    <chakra.h1 fontWeight="bold" fontSize="lg">
                                        &nbsp;&nbsp;&nbsp;AVALANCHE
                                    </chakra.h1>
                                </Flex>
                            </Box>
                        </Flex>
                        </HStack> 
                        <Box textAlign={{ lg: "center" }}>
                            <chakra.h2
                                color={useColorModeValue("brand.600", "brand.600")}
                                fontWeight="semibold"
                                textTransform="uppercase"
                                letterSpacing="wide"
                            >
                                Transactions
                            </chakra.h2>
                            <chakra.p
                                mt={2}
                                fontSize={{ base: "3xl", sm: "4xl" }}
                                lineHeight="8"
                                fontWeight="extrabold"
                                letterSpacing="tight"
                                color={useColorModeValue("gray.900", "brand.600")}
                            >
                                A better way to send money
                            </chakra.p>
                            <chakra.p
                                mt={4}
                                maxW="2xl"
                                fontSize="xl"
                                mx={{ lg: "auto" }}
                                color={useColorModeValue("gray.500", "gray.400")}
                            >
                                Lorem ipsum dolor sit amet consect adipisicing elit. Possimus
                                magnam voluptatum cupiditate veritatis in accusamus quisquam.
                            </chakra.p>
                        </Box>

                        <Box mt={10}>
                            <Stack
                                spacing={{ base: 10, md: 0 }}
                                display={{ md: "grid" }}
                                gridTemplateColumns={{ md: "repeat(2,1fr)" }}
                                gridColumnGap={{ md: 8 }}
                                gridRowGap={{ md: 10 }}
                            >
                                <Feature
                                    title="Competitive exchange rates"
                                    icon={
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"
                                        />
                                    }
                                >
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Maiores impedit perferendis suscipit eaque, iste dolor
                                    cupiditate blanditiis ratione.
                                </Feature>

                                <Feature
                                    title=" No hidden fees"
                                    icon={
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M3 6l3 1m0 0l-3 9a5.002 5.002 0 006.001 0M6 7l3 9M6 7l6-2m6 2l3-1m-3 1l-3 9a5.002 5.002 0 006.001 0M18 7l3 9m-3-9l-6-2m0-2v2m0 16V5m0 16H9m3 0h3"
                                        />
                                    }
                                >
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Maiores impedit perferendis suscipit eaque, iste dolor
                                    cupiditate blanditiis ratione.
                                </Feature>

                                <Feature
                                    title="Transfers are instant"
                                    icon={
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M13 10V3L4 14h7v7l9-11h-7z"
                                        />
                                    }
                                >
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Maiores impedit perferendis suscipit eaque, iste dolor
                                    cupiditate blanditiis ratione.
                                </Feature>

                                <Feature
                                    title="Mobile notifications"
                                    icon={
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="2"
                                            d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                                        />
                                    }
                                >
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Maiores impedit perferendis suscipit eaque, iste dolor
                                    cupiditate blanditiis ratione.
                                </Feature>
                            </Stack>
                        </Box>
                    </Box>
                </Box>
            </Flex>*/}
            <AccountModal isOpen={isOpen} onClose={onClose} />

        </Layout>
    )
}

