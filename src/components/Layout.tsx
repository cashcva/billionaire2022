import React,{ ReactNode } from "react";
import { Box, Flex } from "@chakra-ui/react";

type Props = {
  children?: ReactNode;
};

export default function Layout({ children }: Props) {
  return (
    <React.Fragment>
        <Box pos="relative">
        {children}
        </Box>
    </React.Fragment >
  )
}